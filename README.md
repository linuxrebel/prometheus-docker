### To build prometheus from source
  * clone this repo https://github.com/prometheus/prometheus
  * If you have a working go env you can also do "go get github.com/prometheus/prometheus" without the quotes
  * cd into the clone directory and type make

### For building the Ubuntu Version with ping 
  * clone this repo https://gitlab.com/linuxrebel/prometheus-docker
  * copy the PDockerfile and the pinger dir into the directory where you have previously built prometheus
  * copy the pinger/Makefile.common on top of your Makefile.common
  * run the command  "make docker-ping" without the quotes  

The newly built container will be in your docker images as normal

